# Mirror Lobby & Matchmaking

Hi! This repo is the demo project used in the [Mirror Lobby & Matchmaking tutorial series](https://www.youtube.com/watch?list=PLDI3FQoanpm1X-HQI-SVkPqJEgcRwtu7M&v=k-s54Ix8vmA) on Youtube by [Jared Brandjes](http://www.makegamesplay.games/). 

This project is updated periodically with compatibility fixes for the latest versions of Mirror as well as the occasional addition of further examples as requested on [Discord](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbmYtdzJhbGhIM1ROVVBCNkFhVGRJd2FpcVU1Z3xBQ3Jtc0ttY0dMYWVEb3VYUlQ2LW1BQ1NZRkduY1phTmoxLV9ZRmF0c0FXTV84Yzd3eXA1WFBueTlvd3NnWjJhTFQ1d0ktLWw2TmZ5V2R0Tzhfd0NmRDZTaDFNWHVYNGJoTkNTam1DVGpCUXh3TEFfZGc5dmk5Yw&q=https://discord.gg/kgVrKhqWfq).

Requests, support and suggestions are very welcome over at my Discord channel.

If you find this useful and would like to buy me a ☕ or a 🍺, you can do so with the link below.
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/L4L22U3JS)
